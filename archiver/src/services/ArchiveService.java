package services;

public interface ArchiveService {

    void zipDirectoryWithFiles(String pathToZip, String nameOfZip);
    void unzipAZip(String pathToUnZip, String nameOfZip);
    void uploadAFileToZip(String nameOfZip, String nameOfFile, String pathOfInput, String pathToZip);
    void deleteAFileFromZip(String nameOfFile, String nameOfZipFile);
}
