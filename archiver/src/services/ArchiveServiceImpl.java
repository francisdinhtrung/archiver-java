package services;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

public class ArchiveServiceImpl implements ArchiveService {

    @Override
    public void zipDirectoryWithFiles   (String pathToZip, String nameOfZip) {

        if (pathToZip.isBlank()) { return; }

        if (nameOfZip.isBlank()) { return; }

        Path pathFrom = Paths.get(pathToZip);

        if (pathFrom.getParent() == null) { return; }

        var zipPath = pathFrom.resolve(nameOfZip + ".zip");

        try (var zos = new ZipOutputStream(new BufferedOutputStream(Files.newOutputStream(zipPath)))) {

            Files.walk(pathFrom).forEach(path -> {
                try {
                    var reliativePath = pathFrom.relativize(path);
                    var file = path.toFile();
                    if (file.isDirectory()) {
                        var files = file.listFiles();
                        if (files == null || files.length == 0) {
                            zos.putNextEntry(new ZipEntry(reliativePath.toString() + File.separator));
                            zos.closeEntry();
                        }
                    } else {
                        zos.putNextEntry(new ZipEntry(reliativePath.toString()));
                        zos.write(Files.readAllBytes(path));
                        zos.closeEntry();
                    }
                } catch(IOException e) {
                    e.printStackTrace();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void unzipAZip(String pathToUnZip, String nameOfZip) {
        var outputPath = Path.of(pathToUnZip + nameOfZip);

        try (var zf = new ZipFile(nameOfZip)) {
            if (Files.exists(outputPath)) {
                //TODO: should ask user to allow override, in this case only delete all children.
                Files.walk(outputPath).sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);
            }

            Enumeration<? extends ZipEntry> zipEntries = zf.entries();
            zipEntries.asIterator().forEachRemaining(entry -> {
                try {
                    if (entry.isDirectory()) {
                        var dirToCreate = outputPath.resolve(entry.getName());
                        Files.createDirectories(dirToCreate);
                    } else {
                        var fileToCreate = outputPath.resolve(entry.getName());
                        Files.copy(zf.getInputStream(entry), fileToCreate);
                    }
                } catch(IOException ei) {
                    ei.printStackTrace();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void uploadAFileToZip(String nameOfZip, String nameOfFile, String pathOfInput, String pathToZip) {
        var inputDataDir = Path.of(pathOfInput);
        var zippedDir = Path.of(pathToZip);
        var inputPath = inputDataDir.resolve(nameOfFile);
        var zipPath = zippedDir.resolve(nameOfZip);

        try (var zos = new ZipOutputStream(
            new BufferedOutputStream(Files.newOutputStream(zipPath)))) {
            //TODO: write something in that file, we can see the change before and after zip.
            Files.writeString(inputPath, "I have zipped!\n");

            zos.putNextEntry(new ZipEntry(inputPath.toString()));
            Files.copy(inputPath, zos);
            zos.closeEntry();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteAFileFromZip(String nameOfFile, String nameOfZipFile) {

        /* Define ZIP File System Properies in HashMap */
        Map<String, String> zip_properties = new HashMap<>();
        /* We want to read an existing ZIP File, so we set this to False */
        zip_properties.put("create", "false");
        /* Specify the path to the ZIP File that you want to read as a File System */
        Path pathOfZipFile = Path.of(nameOfZipFile);
        URI zip_disk = URI.create("jar:file:/" + pathOfZipFile);

        /* Create ZIP file System */
        try (FileSystem zipfs = FileSystems.newFileSystem(zip_disk, zip_properties)) {
            /* Get the Path inside ZIP File to delete the ZIP Entry */
            Path pathInZipfile = zipfs.getPath(nameOfFile);
            System.out.println("About to delete an entry from ZIP File" + pathInZipfile.toUri() );
            /* Execute Delete */
            Files.delete(pathInZipfile);
            System.out.println("File successfully deleted");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
